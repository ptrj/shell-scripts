#!/usr/bin/env bash

DOMAIN="${1:-google.com}"
if [ -z $1 ]; then
    USG="  Usage: $0 {Domain Name}"
fi
declare -A DNS_SERVERS
# https://www.lifewire.com/free-and-public-dns-servers-2626062
DNS_SERVERS=( [Google#1]=8.8.8.8 [Google#2]=8.8.4.4 \
            [CloudFlare#1]=1.1.1.1 [CloudFlare#2]=1.0.0.1 \
            [Quad9]=9.9.9.9 \
            [OpenDNS#1]=208.67.222.222 [OpenDNS#2]=208.67.220.220 \
            [Lever3#1]=209.244.0.3 [Level3#2]=209.244.0.4 \
            [DNS.Watch#1]=84.200.69.80 [DNS.Watch#2]=84.200.70.40 \
            [FreeDNS#1]=37.235.1.174 [FreeDNS#2]=37.235.1.177 \
            [Yandex.DNS#1]=77.88.8.8 [Yandex.DNS#2]=77.88.8.1 \
            [ComodoDNS#1]=8.26.56.26 [ComodoDNS#2]=8.20.247.20 )

## Colors
NORM="\e[0m"
LRED="\e[91m"
LYELLOW="\e[93m"
LGREEN="\e[92m"
LBLUE="\e[94m"

check_ip() {
    IP=$(dig +tries=1 +time=3 +short A @${DNS_SERVERS[$DNS_SERVER]} $DOMAIN)
    if [[ "$IP" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        echo -e "  $DNS_SERVER:\t${LGREEN}${IP}${NORM}"
    elif [ -z "$IP" ]; then
        echo -e "  $DNS_SERVER:\t${LRED}N/A${NORM}"
    else
        echo -e "  $DNS_SERVER:\t${LYELLOW}Time out${NORM}"
    fi
}

echo
echo $USG
echo -e "  Domain:\t${LBLUE}${DOMAIN}${NORM}"
for DNS_SERVER in ${!DNS_SERVERS[@]}; do
    check_ip
done
echo